@echo off

rem Oleksandr Dziuba 

cls

:: Chemin du dossier à vérifier/créer
set "folder_path=%userprofile%\desktop\dossier"

:: Vérifie si le dossier existe
if exist "%folder_path%" (
    echo Le répertoire existe déjà sur le bureau.
) else (
    :: Tentative de création du dossier
    md "%folder_path%" >nul 2>&1
    if errorlevel 1 (
        echo Échec de la création du dossier. Veuillez vérifier vos permissions.
    ) else (
        echo Dossier créé avec succès sur le bureau.
    )
)

echo.
pause

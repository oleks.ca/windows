@echo off                           :: Supprime l'affichage des commandes dans la console pour rendre l'exécution propre.

rem Oleksandr Dziuba

title matrix                        :: Définit le titre de la fenêtre de la console à 'matrix'.
color 0A                            :: Définit la couleur du texte à vert sur fond noir.
mode con cols=100 lines=35          :: Configure la console pour qu'elle ait 100 colonnes et 35 lignes.
goto greeting                       :: Dirige l'exécution vers l'étiquette 'greeting'.

:greeting
cls                                 :: Efface le contenu de la console.
echo hello Neo                      :: Affiche le message 'hello Neo'.
pause                               :: Met en pause l'exécution du script et attend que l'utilisateur appuie sur une touche.
goto matrix                         :: Dirige l'exécution vers l'étiquette 'matrix'.

:matrix
echo %random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%%random%
                                   :: Affiche une longue chaîne de nombres aléatoires pour simuler l'effet "Matrix".
goto matrix                        :: Répète indéfiniment l'affichage de la chaîne de nombres aléatoires.

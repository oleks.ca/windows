@echo off

rem Oleksandr Dziuba 04.04.2017

cls
echo.
:menu
echo a allez sous menu
echo b ouvrir le site http://www.google.com
echo q quitter le programme

rem selection
set select=
set /p select=entrez votre choix
if /i %select%=="" goto menu
if /i %select% equ a goto sousmenu
if /i %select% equ b goto site
if /i %select% equ q goto quitter
goto menu

:sousmenu
echo d afficher le dossier
echo e afficher le nom d utilisateur
echo m retour au menu
set select=
set /p select=entrez votre choix
if /i %select%=="" goto menu
if /i %select% equ d goto structure
if /i %select% equ e goto nom
if /i %select% equ m goto menu
goto menu
:structure
dir /ad c:\
pause
goto menu
:nom
net user
pause
goto menu


:site
start http://www.google.com
goto menu

:quitter
exit

